---
---

 function siteSearch() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("query");
    console.log(input);
    filter = input.value.toUpperCase();

    li = document.getElementsByTagName("article");
    console.log(li);
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("div")[0];
        console.log(a);
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

$(document).keyup(function(event) {
    if ($("#query").is(":focus") && event.key == "Enter") {
        siteSearch()
    }
});
